@extends('layouts.admin')

@section('titulo','Área Administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Usuário</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Nome</th>
                        <td>Nome do Usuário</td>
                    </tr>
                    <tr>
                        <th width="150">E-mail</th>
                        <td>usuario@teste.com</td>
                    </tr>
                    <tr>
                        <th width="150">Permissão</th>
                        <td>colaborador</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Ativo</td>
                    </tr>
                    <tr>
                        <th width="150">Foto</th>
                        <td>nome_arquivo.jpg</td>
                    </tr>
                    <tr>
                        <th width="150">Perfil</th>
                        <td>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur odit error exercitationem sapiente culpa obcaecati aperiam deleniti sint, veniam, dolorem officiis libero, aut iusto ipsam nisi deserunt facere esse magnam?</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Usuário</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>
@endsection