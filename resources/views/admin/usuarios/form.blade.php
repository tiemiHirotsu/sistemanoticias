<form action="" method="POST">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="nome">Nome</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-10">
            <input class="form-control" type="text" id="nome" name="titulo" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">E-mail</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-10">
            <input class="form-control" type="email" id="email" name="email" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="password">Senha</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-10">
            <input class="form-control" type="password" id="password" name="password" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="permissao">Permissao</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-5">
            <select name="status" class="form-control" id="permissao">
                <option value="0">Colaborador</option>
                <option value="1">Editor</option>
                <option value="2">Administrador</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="status">Status</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-5">
            <select name="status" class="form-control" id="status">
                <option value="0">Inativo</option>
                <option value="1">Ativo</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="foto">Foto</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-10">
            <input type="file" id="foto" name="foto" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="perfil">Perfil</label>
        {{-- value é o termo que fica como default --}}
        {{-- for está ligado com o id --}}
        <div class="col-sm-10">
        <img src="{{asset('img/perfil.jpg')}}">
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</form>