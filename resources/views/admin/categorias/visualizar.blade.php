@extends('layouts.admin')

@section('titulo','Área Administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Categorias</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Nome</th>
                        <td>Nome da categoria</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>ativo</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Usuário</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>
@endsection