@extends('layouts.admin')

@section('titulo','Área Administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Notícia</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Título</th>
                        <td>Título</td>
                    </tr>
                    <tr>
                        <th width="150">Sub-título</th>
                        <td>sub-título</td>
                    </tr>
                    <tr>
                        <th width="150">Descrição</th>
                        <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Explicabo praesentium autem voluptas dolores ipsam similique ut, non amet quos quod odio officiis quam dignissimos, odit nostrum perferendis ratione aut accusantium.</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Não publicado</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Notícia</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>
@endsection