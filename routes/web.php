<?php
// -----------------------------------Seção de HOME
Route::get('/', function () {
    return view('home.index');
});

Route::get('/contato', function () {
    return view('home.contato');
});


// -----------------------------------Seção de notícias
Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});


// ------------------------------------Seção do administrador!
Route::get('/admin/home',function(){
    return view('admin.home.index');
});

//-------------------------------------seção Admin/noticias
Route::get('/admin/noticias','Admin\NoticiasController@index');

Route::get('/admin/noticias/cadastrar','Admin\NoticiasController@cadastrar');

Route::get('/admin/noticias/editar','Admin\NoticiasController@editar');

Route::get('/admin/noticias/visualizar','Admin\NoticiasController@visualizar');

Route::get('/admin/noticias/deletar','Admin\NoticiasController@deletar');


// barra invertida quando lida com Classe!!
//seção admin/categorias
Route::get('/admin/categorias','Admin\CategoriasController@index');

Route::get('/admin/categorias/cadastrar','Admin\CategoriasController@cadastrar');

Route::get('/admin/categorias/editar','Admin\CategoriasController@editar');

Route::get('/admin/categorias/visualizar','Admin\CategoriasController@visualizar');

Route::get('/admin/categorias/deletar','Admin\CategoriasController@deletar');


//seção admin/usuarios
Route::get('/admin/usuarios','Admin\UsuariosController@index');

Route::get('/admin/usuarios/cadastrar','Admin\UsuariosController@cadastrar');

Route::get('/admin/usuarios/editar','Admin\UsuariosController@editar');

Route::get('/admin/usuarios/visualizar','Admin\UsuariosController@visualizar');

Route::get('/admin/usuarios/deletar','Admin\UsuariosController@deletar');
